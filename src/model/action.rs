/// Predefined actions for the program to take for a given journ CLI command
#[derive(Debug)]
pub enum Action {
    Init,
    Edit,
    New,
    Help,
    Insert,
    Report,
    Archive,
    Version,
}
