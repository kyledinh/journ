use crate::model::Action;

#[derive(Debug)]
pub struct Req {
    pub args: Vec<String>,
    pub payload: String,
    pub action: Action,
}

impl Req {
    pub fn new(args: Vec<String>, action: Action) -> Req {
        let payload = get_payload(args.clone());
        Req {
            args: args,
            payload: payload,
            action: action,
        }
    }
}

// Takes the params after <cmd> <action> [param param] as the payload
fn get_payload(args: Vec<String>) -> String {
    let last = args.len();
    if args.len() > 2 {
        let params = &args[2..last];
        return params.join(" ");
    }
    "".to_string()
}
