use std::{env};
use model::{Action};

mod api;
mod model;
mod util;

fn main() {
    api::check_user_setup(false);
    let args: Vec<String> = env::args().collect();
    let req = api::process_args(args);
    //println!("This is the req: {:?} for {:?}", req.action, req.payload);

    match req.action {
        Action::Init => api::init(),
        Action::Insert => api::insert(req),
        Action::New => api::new(req),
        _ => api::help(req),
    }
}
