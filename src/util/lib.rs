use chrono::prelude::*;
use chrono::{Datelike};

use std::fs;
use std::io::{BufRead, BufReader, Write};
use std::fs::{File, OpenOptions};

pub fn get_isoweek_signature(local: DateTime<Local>) -> String {
    let week: String  = if local.iso_week().week() < 10 {
        format!("W0{}", local.iso_week().week())
    } else {
        format!("W{}", local.iso_week().week())
    };
    format!("{}-{}-{}", local.iso_week().year(), week, local.weekday().number_from_monday())
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     #[test]
//     fn test_001() {
//         let nd = DateTime::parse_from_str("2020-01-04 07:20:42 -08:00","%Y-%m-%d %H:%M:%S %z");
//         // assert_eq!(nd.iso_week().week(), 1);
//         // assert_eq!(nd.weekday().number_from_monday(), 6);
//         // let dtl = chrono::offset::local::Local::from_utc_datetime(&self, &nd);
//         assert_eq!(get_isoweek_signature(nd), "2020-W01-6");
//
//     }
// }

// fn trim(string: &str) -> &str {
//     string.trim_start_matches('#').trim_matches(' ')
// }
//

fn title_string<R>(mut reader: R) -> String
    where R: BufRead, {
    let mut first_line = String::new();

    reader.read_line(&mut first_line).expect("Unable to read line");
    let last_hash = first_line
        .char_indices()
        .skip_while(|&(_, c)| c == '#')
        .next()
        .map_or(0, |(idx, _)| idx);

    // Trim the leading hashes and any whitespace
    first_line[last_hash..].trim().into()
}

pub fn read_isoweek_sig_from_today_md(path: String) -> String {
    let file = match fs::File::open(&path) {
        Ok(file) => file,
        Err(_) => panic!("Unable to read title from {}", path),
    };

    let buffer = BufReader::new(file);
    title_string(buffer)
}

pub fn write_new_today_md(path: String, output: String) {
    let mut ofile = File::create(&path)
        .expect("unable to create file");
    ofile.write_all(output.as_bytes()).expect("unable to write");
}

pub fn write_append_today_md(path: String, entry: String) {
    let mut file = OpenOptions::new()
        .append(true)
        .open(&path)
        .unwrap();

    if let Err(_) = writeln!(file, "{}", &entry) {
        eprintln!("Couldn't write to file: ");
    }
}

pub fn move_today_md(from: String, to: String) {
    println!("DEBUG: from {} to {}", from, to);
    match fs::rename(from, &to) {
        Ok(_) => { },
        Err(e) => panic!("Unable to move today.md to {}, Error: {} ", to, e),
    }
}
