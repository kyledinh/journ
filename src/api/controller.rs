use std::{fs, process};
use crate::model::action::Action;
use crate::model::req::Req;
use shellexpand;


pub fn process_args(args: Vec<String>) -> Req {
    if args.len() > 1 {
        let action = match args[1].as_ref() {
            "." => Req::new(args, Action::Insert),
            "init" => Req::new(args, Action::Init),
            "-v" => Req::new(args, Action::Version),
            "archive" => Req::new(args, Action::Archive),
            "edit" => Req::new(args, Action::Edit),
            "help" => Req::new(args, Action::Help),
            "new" => Req::new(args, Action::New),
            "report" => Req::new(args, Action::Report),
            _ => Req::new(args, Action::Help),
        };
        return action
    }

    println!("Requires arguments. USEAGE: journ . another note to my file");
    Req::new(args, Action::Help)
}

fn create_dir(dir_str: String) {
    match fs::create_dir(&dir_str) {
        Ok(_) => { println!("Directory {} created!", &dir_str) },
        Err(e) => { println!("FAILED TO CREATE! Maybe permissions issue? {}", e); process::exit(1); },
    }
}

pub fn check_user_setup(create: bool) {
    let home_cfg_dir_journ = shellexpand::tilde("~/.journ");
    if std::path::Path::new(&home_cfg_dir_journ.to_string()).exists() {
        if create { println!("~/.journ exists") }
    } else {
        println!("~/.journ NOT THERE");
        if create {
            create_dir(home_cfg_dir_journ.to_string());
            println!("Journ config directory created.");
        } else {
            println!("you need to run `journ init`");
            process::exit(1);
        }
    }

    let home_dir_journ = shellexpand::tilde("~/journ");
    if std::path::Path::new(&home_dir_journ.to_string()).exists() {
        if create { println!("~/journ exists") }
    } else {
        println!("~/journ NOT THERE");
        if create {
            create_dir(home_dir_journ.to_string());
            println!("Journ home directory created.");
        } else {
            println!("you need to run `journ init`");
            process::exit(1);
        }
    }
}
