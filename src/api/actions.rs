use super::*;
use crate::model::req::Req;
use crate::util;
use shellexpand;
use chrono::prelude::*;


pub fn help(req: Req) {
    let local: DateTime<Local> = Local::now();
    let isoweek_sig = util::get_isoweek_signature(local);
    println!("HELP: Journ is iso 8601 format date journal that uses md format. Local time {:?}", local);
    println!("ISOWEEK: {}.md", isoweek_sig);
    println!("This is the req: {:?} for {:?}", req.action, req.payload);
}

pub fn init() {
    println!("INIT MODE, will create ~/journ directory if it does not exist.");
    check_user_setup(true);
}

pub fn new(req: Req) {
    println!("NEW MODE, will create a new today.md file.");
    let today_md = shellexpand::tilde("~/journ/today.md");
    if std::path::Path::new(&today_md.to_string()).exists() {
        let today_isoweek_sig = util::read_isoweek_sig_from_today_md(today_md.to_string());
        let isoweek_md = shellexpand::tilde(&("~/journ/".to_owned() + &today_isoweek_sig + &".md".to_owned())).to_string();
        println!("today.md file exists! With {}", today_isoweek_sig);
        // Move this today.md to isoweek_sig.md
        util::move_today_md(today_md.to_string(), isoweek_md.to_string());
    }

    let local: DateTime<Local> = Local::now();
    let isoweek_sig = util::get_isoweek_signature(local);
    let sig = "# ".to_owned() + &isoweek_sig + &"\n".to_owned();
    let entry = "\n* ".to_owned() + &req.payload + &"\n".to_owned();
    println!("today.md file DOES NOT exists! Will create {}", sig);
    if req.payload.len() > 0 {
        util::write_new_today_md(today_md.to_string(), sig.to_string() + &entry.to_string());
    } else {
        util::write_new_today_md(today_md.to_string(), sig.to_string());
    }
}

pub fn insert(req: Req) {
    println!("INSERT MODE, will append to today.md file.");
    let today_md = shellexpand::tilde("~/journ/today.md");
    if std::path::Path::new(&today_md.to_string()).exists() {
        let entry = "* ".to_owned() + &req.payload;
        let today_isoweek_sig = util::read_isoweek_sig_from_today_md(today_md.to_string());
        println!("Appending today.md {} with new entry", today_isoweek_sig);
        util::write_append_today_md(today_md.to_string(), entry);
    } else {
        let local: DateTime<Local> = Local::now();
        let isoweek_sig = util::get_isoweek_signature(local);
        let sig = "# ".to_owned() + &isoweek_sig + &"\n\n".to_owned();
        let entry = "* ".to_owned() + &req.payload + &"\n".to_owned();
        println!("today.md file DOES NOT exists! Will create {}", sig);
        util::write_new_today_md(today_md.to_string(), sig.to_string() + &entry.to_string());
    }
}
