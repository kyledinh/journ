# Journ
> Quick journaling in Markdown and ISO8601 weekly format

## Development

* `cargo build` 

## Usage

* `journ init` - creates the `journ` home directory
* `journ new` - creates a new `today.md` and archives the current version
* `journ . <entry>` - appends `today.md` with an entry

